var app= new Vue({
    el: '#totoro',
    data: {
        searchPokemon : "",
        pokemon:{
            name: "Inconnu",
            stats : []
        }
    },
    mounted(){
        /*window.axios.get('https://pokeapi.co/api/v2/pokemon/1/').then(response => {
            console.log(response.data)
            app.pokemon.name = response.data.species.name
            stats = response.data.stats
            stats.forEach(stat =>
            {
                app.pokemon.stats.push([stat.stat.name,stat.base_stat])
            })
        })*/
    },
    methods: {
        searchPokemonButton()
        {
            window.axios.get(`https://pokeapi.co/api/v2/pokemon/${app.searchPokemon}/`).then(response => {
                console.log(response.data)
                app.pokemon.name = response.data.species.name
                stats = response.data.stats
                stats.forEach(stat =>
                {
                    app.pokemon.stats.push([stat.stat.name,stat.base_stat])
                })
            })
        }
    }
});