Vue.component('compteur', {
    data: () => {
        return {
            comptage:0
        }
    },
    template: '<button v-on:click="comptage++"> vous m\'avez cliqué {{comptage}} fois.</button>'
})

Vue.component('afaire-item', {
    props:{
        liste: Object
    },
    template: '<li>{{liste.code}} - {{liste.libelle}}</li>'
})

const app = new Vue({
    el: "#testComponant",
    data (){
        return {
            listeSlam: [
                {
                    id: 1,
                    code: 'SLAM3',
                    libelle: 'Data'
                },
                {
                    id: 2,
                    code: 'SLAM4',
                    libelle: 'Dev'
                },
                {
                    id: 3,
                    code: 'PPE',
                    libelle: 'Projet'
                }
            ]
        }
    }
})