Vue.component('competence', {
    props: ['titre','contenu','detail'],
    data() {
        return {
            voirCompetence: true,
            voirDetail: false
        }
    },
    template: `
        <article class="message" v-show="voirCompetence">
            <div class="message-header">
                {{ titre }}
                <button type="button" @click="voirCompetence = false">x</button>
            </div>
            <div class="message-body">
                {{ contenu }}
            </div>
            <modal v-if="voirDetail" @close="voirDetail=false">{{ detail }}</modal>
            <button @click="voirDetail=true">Voir détails</button>
        </article>
    `
})

Vue.component('modal', {
    template: `
        <div class modal="is-active">
            <div class="modal-background"></div>
            <div class="modal-content">
                <div class="box">
                    <p>
                        <slot></slot>
                    </p>
                </div>
            </div>
            <button class="modal-close" @click="$emit('close')"></button>
        </div>
        `
})

var app = new Vue({
    el: '#root',
    data: {
        montreDetail: false
    }
})
